# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import LightOnEvent, LightOffEvent

from .action import Action3
from mud.events import EmbraserEvent, CreerFeuEvent

class LightOnAction(Action2):
    EVENT = LightOnEvent
    ACTION = "light-on"
    RESOLVE_OBJECT = "resolve_for_use"

class LightOffAction(Action2):
    EVENT = LightOffEvent
    ACTION = "light-off"
    RESOLVE_OBJECT = "resolve_for_use"

class EmbraserAction(Action3):
    EVENT = EmbraserEvent
    ACTION = "embraser"
    RESOLVE_OBJECT = "resolve_for_use"

class CreerFeuAction(Action3):
    EVENT = CreerFeuEvent
    ACTION = "creerfeu"
    RESOLVE_OBJECT = "resolve_for_use"
